from transformers import AutoTokenizer, AutoModelForSeq2SeqLM


#model.to('cuda')




class modelSpining1:
    def __init__(self):
        self.tokenizer = AutoTokenizer.from_pretrained("/home/alpha/Downloads_models/spining")
        #self.tokenizer = AutoTokenizer.from_pretrained("Vamsi/T5_Paraphrase_Paws")
        self.model = AutoModelForSeq2SeqLM.from_pretrained("/home/alpha/Downloads_models/spining")
        #self.model = AutoModelForSeq2SeqLM.from_pretrained("Vamsi/T5_Paraphrase_Paws")


    def Textspining(self,sentence):
        #sentence = "Lest you miss out on the delightful goings-on in U.S. politics these days, picture this: A politician passes a draconian, poorly thought-out, maybe illegal regulation, designed mostly as a symbolic gesture to his base but that will likely help none of his actual constituents. Then, a pro-business lobby jumps in to decry the law as a threat to society and reflexively reminds said politician that he’s not supposed to pass any laws that impact the wallets of the lobby’s client list. Yes, it’s American democracy, well-oiled machine that it is, chugging along and hard at work."

        text =  "paraphrase: " + sentence + " </s>"

        encoding = self.tokenizer.encode_plus(text,padding='longest', return_tensors="pt")
        #input_ids  = encoding["input_ids"].to("cuda")
        input_ids  = encoding["input_ids"]
        #attention_masks=encoding["attention_mask"].to("cuda")
        attention_masks=encoding["attention_mask"]
        outputs = self.model.generate(
            input_ids=input_ids,
            attention_mask=attention_masks,
            max_length=256,
            do_sample=True,
            top_k=200,
            top_p=0.95,
            early_stopping=True,
            num_return_sequences=3)

        spinnedTexts=[]
        for output in outputs:
            line = self.tokenizer.decode(output, skip_special_tokens=True,clean_up_tokenization_spaces=True)
            spinnedTexts.append(line)
            #print(line)


        return spinnedTexts

"""

if __name__=="__main__":
    print("start test spinning ")
    
    a=modelSpining1()
    res=a.Textspining(text)

    print("original phrase :\n")
    print(text)

    print("\n Results ")
    for i in res:
        print(i)
        print("\n\n")
        
"""