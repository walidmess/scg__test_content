# importing editor from movie py
from moviepy.editor import *
import random
#Set assets paths
ImagesPath="C:\\Users\\wezzy\\PycharmProjects\\SmartContentCreation\\VideoCreation\\Images\\"
AudioPath="C:\\Users\\wezzy\\PycharmProjects\\SmartContentCreation\\VideoCreation\\Audio\\"

#Set supported formats
SupportedAutioFormat=['ra', 'aif', 'aiff', 'aifc', 'wav', 'au', 'snd', 'mp3', 'mp2']
SupportedImagesFormat=['ras', 'xwd', 'bmp', 'jpe', 'jpg', 'jpeg', 'xpm', 'ief', 'pbm', 'tif', 'gif', 'ppm', 'xbm', 'tiff', 'rgb', 'pgm', 'png', 'pnm']
SupportedVideoFormat=['m1v', 'mpeg', 'mov', 'qt', 'mpa', 'mpg', 'mpe', 'avi', 'movie', 'mp4']


#Convert images to small clips
#Imageclips = [ImageClip(ImagesPath+i).set_duration(2)  for i in os.listdir(ImagesPath) if i.split('.')[-1] in SupportedImagesFormat+SupportedVideoFormat ]
Imageclips=[]
for image in os.listdir(ImagesPath):
    #get only images and videos
    if image.split('.')[-1] in SupportedImagesFormat + SupportedVideoFormat:

        clip=ImageClip(ImagesPath + image).set_duration(2)

        #Add text
        txt_clip = TextClip("Rbe7na l'allemagne wahd el 3am", fontsize=30, color='white',bg_color="black")

        # setting position of text in the center and duration will be 5 seconds
        txt_clip = txt_clip.set_pos('left').set_duration(clip.duration)

        # Overlay the text clip on the first video clip
        clip = CompositeVideoClip([clip, txt_clip])
        Imageclips.append(clip)



#Get aviable audio files
AudiFiles = [ AudioPath+i for i in os.listdir(AudioPath) if i.split('.')[-1] in SupportedAutioFormat ]

#Create the video
video = concatenate_videoclips(Imageclips, method="compose")

Audio=AudioFileClip(random.choice(AudiFiles)).subclip(0, video.duration)



video=video.set_audio(Audio)



video.write_videofile("test11.mp4", fps=1)





