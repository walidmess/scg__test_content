import cv2
import numpy as np
import matplotlib.pyplot as plt


logo = cv2.imread("ppp-removebg-preview.png")
print(logo.shape)
plt.imshow(logo)
plt.show()

h_logo, w_logo, _ = logo.shape

img = cv2.imread("TESTimage.jpg")
h_img, w_img, _ = img.shape
plt.imshow(img)
plt.show()



# Get the center of the original. It's the location where we will place the watermark
center_y = int(h_img / 2)
center_x = int(w_img / 2)
top_y = center_y - int(h_logo / 2)
left_x = center_x - int(w_logo / 2)
bottom_y = top_y + h_logo
right_x = left_x + w_logo

roi = img[top_y: bottom_y, left_x: right_x].copy()

# Add the Logo to the Roi
result = cv2.addWeighted(roi, 1, logo,1, 0)
# Replace the ROI on the images
img[top_y: bottom_y, left_x: right_x] = result

newRoi=img[top_y: bottom_y, left_x: right_x]

r_h,r_w,_=roi.shape

"""
for row in range(r_h):
    for col in range(r_w):
        print(roi[row][col],newRoi[row][col])
"""
cv2.imshow("",img)

cv2.waitKey()