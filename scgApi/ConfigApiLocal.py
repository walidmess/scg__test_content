from flask import Flask
from flask_restful import Api

from Collect import collect
from Spinning import spinning
from Summerizing import TextSummerizing
from CreateVidio import createVideo
from test import testHTTP


app=Flask(__name__)
api=Api(app)

api.add_resource(collect, '/collect')
api.add_resource(spinning, '/spinning')
api.add_resource(TextSummerizing, '/TextSummerizing')
api.add_resource(createVideo, '/createVideo')
api.add_resource(testHTTP, '/testHTTP')


if __name__ == '__main__':
    app.run()  # run our Flask app

