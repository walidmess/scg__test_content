import sys,os
sys.path.append('/home/alpha/scg__test_content')

import dotenv

from ContentSource.GlobalAgentCollector.CollectData import GlobalCollector
from datetime import datetime

def CheckOnGoingOperation():


    dotenv_file = dotenv.find_dotenv()
    dotenv.load_dotenv(dotenv_file)
    if os.environ["OnWorking"]=="True":
        print("Previous Operation status ","Ongoing")
    else:
        print("Previous Operation status ","Finished")



    return os.environ["OnWorking"]

def StartOperation():

    dotenv_file = dotenv.find_dotenv()
    dotenv.load_dotenv(dotenv_file)

    os.environ["OnWorking"] = "True"

    print("Locking the operation status with "," OnGoing")


    # Write changes to .env file.
    dotenv.set_key(dotenv_file, "OnWorking", os.environ["OnWorking"])

def EndOperation():

    dotenv_file = dotenv.find_dotenv()
    dotenv.load_dotenv(dotenv_file)

    os.environ["OnWorking"] = "False"

    print("Locking the operation status with "," Finished")
     # outputs "value"

    #print(os.environ['OnWorking'])  # outputs 'newvalue'

    # Write changes to .env file.
    dotenv.set_key(dotenv_file, "OnWorking", os.environ["OnWorking"])





if __name__=="__main__":
    now = datetime.now()

    if CheckOnGoingOperation()=="False":
        StartOperation()

        print("Start collecting at",now)

        try:
            GC=GlobalCollector()
            #GC.ParallelCollecting()
            GC.ParallelArticleCollecting()
        except:
            print("Syetem err caused SCG closing ")
            print("Unlocking the ongoing key ")
            EndOperation()

        EndOperation()


    else:
        print("There is an unfinished previous collecting operation ")
        print("This operation will be ignored ")