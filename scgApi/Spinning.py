from flask_restful import Resource,request

from ContentSource.DatabaseHandler.DB_Handler import db_handler

class spinning(Resource):

    def post(self):

        Paragraph_id=request.args['PragraphID']

        paragraph_content=self.getData(Paragraph_id)

        spinned_paragraph=self.call_spinner(paragraph_content)


        self.save_results(Paragraph_id,spinned_paragraph)



    def getData(self,Paragraph_id):

        dbHandler=db_handler()

        paragraph_content=dbHandler.getParagraph(Paragraph_id)

        return paragraph_content

    def call_spinner(self,paragraph_content):

        from textSpinning.DeeepLearningModels.model1.model1 import modelSpining1

        spinner=modelSpining1()

        result=spinner.Textspining(paragraph_content)


        return  result

    def save_results(self,Paragraph_id,result):


        dbHandler=db_handler()

        for para in result:

            dbHandler.saveSpinnedArticle(Paragraph_id,para)
