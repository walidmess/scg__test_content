from flask_restful import Resource,reqparse,request

from ContentSource.DatabaseHandler.DB_Handler import db_handler


class TextSummerizing(Resource):

    def post(self):

        article_id=request.args['article_id']

        article_content=self.getData(article_id)

        summerized_article=self.call_summerizer(article_content)
        #summerized_article="one two three viva l'algerie"

        self.save_results(article_id,summerized_article)



    def getData(self,article_id):


        dbHandler=db_handler()

        article_content=dbHandler.getArticle(article_id)

        return article_content

    def call_summerizer(self,article_content):

        from textSummerizing.TextSummerizingAgent import summerizerAgent

        summerizer=summerizerAgent()

        result=summerizer.summerize(article_content)

        return  result

    def save_results(self,article_id,result):


        dbHandler=db_handler()

        dbHandler.saveSummerizedText(article_id,result)



