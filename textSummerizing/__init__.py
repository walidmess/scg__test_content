from text2text import Handler

text="""Construction on the factory, which will be called Peloton Output Park, is expected to start later this summer. Peloton Output Park is described in a company press release as a “state-of-the-art factory” and will purportedly feature the “latest in industrial technology and automation,” as well as renewable energy sources. At more than 200 acres in size and more than 1 million square feet of “manufacturing, office, and amenities space,” the factory is on track to be massive. Peloton views the facility as more than a factory: The company also stated it plans to include a fitness center, a team lounge for collaboration, and an on-site showroom. The investment is expected to create roughly 2,000 jobs in the area, and Peloton fans will also be able to take tours of the facility."""


h = Handler([text], src_lang="en")


ee=h.summarize()

print(ee)

