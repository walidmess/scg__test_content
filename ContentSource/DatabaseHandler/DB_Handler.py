from mysql.connector import connect, Error
from textSpinning.DeeepLearningModels.model1.model1 import modelSpining1
import time as TIMEE
import datetime
class db_handler:
    def __init__(self):


        self.LoadServerSettings()

        #self.LoadLocalSettings()

        self.Connect()

        self.spinner=modelSpining1()

    def getParagraph(self, Paragraph_id):

        cursor = self.db.cursor()

        sql = "SELECT `Content` FROM Paragraphs WHERE PragraphID="+str(Paragraph_id)

        val=(Paragraph_id)

        #print("sql : ",sql," id : ",Paragraph_id)

        cursor.execute(sql)

        paragraphContent=cursor.fetchone()[0]

        #print("result :",paragraphContent)


    #if len(cursor.fetchall())>0:
            #paragraphContent = cursor.fetchone()[0]



        return paragraphContent


    def saveArticle(self, url,Cover,category, title, content, time):


        cursor = self.db.cursor()


        save_article_sql = "INSERT INTO Articles (ArticleUrl,ArticleTitle,Cover,Caption,VideoUrl,SubCategory,ArticleElements,ArticleDate,Pushed,Desabled,CreatedAt) VALUES (%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s)"

        val = (url, title,Cover['src'],Cover['caption'],Cover['url'],category, "In progress", time,0,0,datetime.datetime.now())
        cursor.execute(save_article_sql, val)

        StarttingTime = TIMEE.time()



        #print(cursor.rowcount, "article record inserted.")

        #get inserted article id


        articleId=cursor.lastrowid


        ArticleIlements=""

        for element in content:

            if element['type']=="paragraph":

                element["value"]=element["value"].strip()
                if element["value"]!="":
                    inser_parag_sql="INSERT INTO Paragraphs (Content,ArticlesId) VALUES (%s, %s)"

                    val = (element["value"],articleId)
                    cursor.execute(inser_parag_sql, val)
                    #get the id



                    ElementId=cursor.lastrowid


                    ArticleIlements+="{type: paragraph , value: "+str(ElementId)+" },"
                    #print(cursor.rowcount, "paragraph record inserted.")

                    #spin the paraagraph


                    spinned_paragraphs=self.spinner.Textspining(element["value"])

                    #save the spinning results

                    for para in spinned_paragraphs:
                        insert_sql_spinned="INSERT INTO SpinnedArticle (Content,ParagraphId,Used) VALUES (%s, %s,%s)"

                        val_spinned = (para,ElementId,0)
                        cursor.execute(insert_sql_spinned, val_spinned)
                        #print(cursor.rowcount, "paragraph spinned record inserted.")




            elif element['type']=="image":

                inser_img_sql="INSERT INTO Images (Url,ArticlesId,Caption) VALUES (%s, %s,%s)"


                val = (element["value"]["src"],articleId,element["value"]["caption"])

                cursor.execute(inser_img_sql, val)


            #get the id


                ElementId=cursor.lastrowid
                ArticleIlements+="{type: image , value: "+str(ElementId)+" },"
                #print(cursor.rowcount, "Image record inserted.")



        #Update the article table to add elements

        inser_elements_sql="UPDATE Articles set ArticleElements= %s WHERE ArticleID= %s"


        val=(ArticleIlements,str(articleId))
        cursor.execute(inser_elements_sql,val)



        print(cursor.rowcount, "ArticleElement record inserted.")

        #save spinned titles


        Spinnedtitles=self.spinner.Textspining(title)

        for SpinnedTitle in Spinnedtitles:
            save_article_sql = "INSERT INTO SpinnedTitles (ArticlesId,Content,Used) VALUES (%s, %s,%s)"

            val = (articleId,SpinnedTitle,0 )
            cursor.execute(save_article_sql, val)

            #print(cursor.rowcount, "spinned title record inserted.")



         #Save time
        EndingTime = TIMEE.time()
        TotalTime = EndingTime- StarttingTime

        save_Time_sql = "INSERT INTO Logs (Type,Content,CreatedAt) VALUES ( %s,%s,%s)"

        val = ("Spinning",TotalTime,datetime.datetime.now() )
        cursor.execute(save_Time_sql, val)


        #print(cursor.rowcount, "Log record inserted.")
        self.db.commit()



    def LoadServerSettings(self):
        self.pwd="[+XPNj@E5a"
        self.host="192.168.20.30"
        self.db_name="SCG"
        self.user="SCGUSER"

    def LoadLocalSettings(self):
        self.pwd=""
        self.host="localhost"
        self.db_name="SCG"
        self.user="root"


    def saveSpinnedArticle(self, Paragraph_id, spinned_paragraph_content):
         cursor = self.db.cursor()

         sql = "INSERT INTO Paragraphs (Paragraph_id,spinned_paragraph_content) VALUES (%s, %s)"

         val = (Paragraph_id, spinned_paragraph_content)
         cursor.execute(sql, val)

         self.db.commit()

         #print(cursor.rowcount, "spinned record inserted.")

    def getAllUrls(self):
        cursor = self.db.cursor()

        sql="SELECT `ArticleUrl` FROM Articles"

        cursor.execute(sql)



        URls = [url[0] for url in cursor.fetchall()]

        return URls


    # def saveSummerizedText(self, article_id, summerized_text):
    #     cursor = self.db.cursor()

    #     sql = "INSERT INTO text_abstracting (article_id,abstracted_article) VALUES (%s, %s)"

    #     val = (article_id, summerized_text)
    #     cursor.execute(sql, val)

    #     self.db.commit()

    #     print(cursor.rowcount, "summerized text record inserted.")

    def Connect(self):
        self.db = connect(
            host=self.host,
            user=self.user,
            password=self.pwd,
            database=self.db_name
        )


if __name__=="__main__":

    D=db_handler()

    D.getAllUrls()