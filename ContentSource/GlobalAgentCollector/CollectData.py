from ContentSource.CollectAgents.Tech.Wired.WiredAgent import TheWiredAgent
from ContentSource.CollectAgents.Tech.theverge.thevergeAgent import TheVergeAgent
from ContentSource.CollectAgents.Tech.TechCrunch.TechCrunchAgent import TechCrunchAgent
from ContentSource.CollectAgents.Tech.ventureBeat.ventureBeatAgent import TheVentureBeatAgent
from ContentSource.CollectAgents.Tech.gizmodo.gizmodoAgent import GizmodoAgent

from ContentSource.CollectAgents.Tech.theverge.thevergeAgentSimultanesly import TheVergeAgentSimulatanesly

from multiprocessing import Process,Pool,cpu_count
import multiprocessing
import time

import os
os.environ["TOKENIZERS_PARALLELISM"] = "true"

class GlobalCollector:

    def ParallelCollecting(self):
        print("Start parallel Collecting ..")

        start = time.time()
        # A1 = GizmodoAgent("https://gizmodo.com/latest", "Gizmodo")

        # A2 = TheWiredAgent("https://www.wired.com/", "TheWired")

        # A3 = TechCrunchAgent("https://techcrunch.com/", "techCrunch")

        A4 = TheVergeAgent("https://www.theverge.com/tech", "theVerge")

        # A5 = TheVentureBeatAgent("https://venturebeat.com/", "theVentureBeat")

        # t1 = Process(target=A1.StartWorking)
        # t2 = Process(target=A2.StartWorking)
        # t3 = Process(target=A3.StartWorking)
        t4 = Process(target=A4.StartWorking)
        # t5 = Process(target=A5.StartWorking)

        # t1.start()
        # t2.start()
        # t3.start()
        t4.start()
        # t5.start()

        # t1.join()
        # t2.join()
        # t3.join()
        t4.join()
        # t5.join()

        end = time.time()

        print("time = ", str(end - start))
        # both threads completely executed
        print("Done !")

    def SequencialCollecting(self):
        print("Start Sequencial Collecting ..")
        start = time.time()
        GizmodoAgent("https://gizmodo.com/latest", "Gizmodo").StartWorking()

        TheWiredAgent("https://www.wired.com/", "TheWired").StartWorking()

        TechCrunchAgent("https://techcrunch.com/", "techCrunch").StartWorking()

        TheVergeAgent("https://www.theverge.com/tech",
                      "theVerge").StartWorking()

        TheVentureBeatAgent("https://venturebeat.com/",
                            "theVentureBeat").StartWorking()

        end = time.time()

        print("time = ", str(end - start))
        # both threads completely executed
        print("Done !")

    def ParallelArticleCollecting(self):
        print("Start Articles parallel Collecting ..")

        start = time.time()

        websitesList=[["https://www.theverge.com/tech", "theVerge"]]


        for website in websitesList:
            A = TheVergeAgentSimulatanesly(website[0], website[1])

            cleanLinks=A.StartWorking()

            if len(cleanLinks)>0:

                with multiprocessing.get_context('spawn').Pool(cpu_count()) as pool:

                    #P=Pool(cpu_count())
                    pool.map(A.ParalelWorker,cleanLinks)






        end = time.time()

        print("time = ", str(end - start))
        # both threads completely executed
        print("Done !")
