from ContentSource.CollectAgents.Tech.Wired.WiredAgent import TheWiredAgent
from ContentSource.CollectAgents.Tech.theverge.thevergeAgent import TheVergeAgent
from ContentSource.CollectAgents.Tech.TechCrunch.TechCrunchAgent import TechCrunchAgent
from ContentSource.CollectAgents.Tech.ventureBeat.ventureBeatAgent import TheVentureBeatAgent
from ContentSource.CollectAgents.Tech.gizmodo.gizmodoAgent import GizmodoAgent

import time

if __name__ == '__main__':
    start = time.time()
    A1 = GizmodoAgent("https://gizmodo.com/latest", "Gizmodo").StartWorking()

    A2 = TheWiredAgent("https://www.wired.com/", "TheWired").StartWorking()

    A3 = TechCrunchAgent("https://techcrunch.com/", "techCrunch").StartWorking()

    A4 = TheVergeAgent("https://www.theverge.com/tech", "theVerge").StartWorking()

    A5 = TheVentureBeatAgent("https://venturebeat.com/", "theVentureBeat").StartWorking()

    end = time.time()

    print("time = ", str(end - start))
    # both threads completely executed
    print("Done !")



