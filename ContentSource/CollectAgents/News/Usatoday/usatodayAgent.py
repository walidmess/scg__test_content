
from ContentSource.CollectAgents.BaseCollectingAgent.SmartAgent import SmartAgent
import traceback


class UsaTodayAgent(SmartAgent):

    def ScanHomePage(self):



        Articles = self.browser.find_elements_by_tag_name("a")

        self.ArticlesLinks =[]

        for i in Articles:
            if i.get_attribute("href").split("/")[3] =="story":

                if "reviewed" not in i.get_attribute("href").split("/"):
                    self.ArticlesLinks.append(i.get_attribute("href"))



    def ReadArticle(self, link):

        self.browser.get(link)

        ArticleData = dict()

        ArticleData['link']=link
        ArticleData['title'] = self.browser.find_element_by_tag_name("h1").text

        ArticleData['date'] = self.browser.find_element_by_xpath("//div[@class='gnt_ar_dt']").get_attribute("aria-label").replace('Published:','')

        if "Updated:" in ArticleData["date"].split(' '):
            ArticleData["date"]=ArticleData["date"].split("Updated:")[0]


        ArticleData['category'] = link.split("/")[4]

        ArticleData['cover'] = self.getCoverImage()

        list_article_children=self.browser.find_element_by_xpath("//div[@class='gnt_ar_b']").find_elements_by_xpath("*")


        ArticleData['content'] = []


        for element in list_article_children:

            if element.tag_name=="p":
                articleElement = dict()

                articleElement['type'] = "paragraph"
                articleElement['value'] = element.text
                ArticleData['content'].append(articleElement)

            elif element.tag_name in  ["h1","h2","h3","h4"]:
                articleElement = dict()
                articleElement['type'] = "title"
                articleElement['size'] = element.tag_name
                articleElement['value'] = element.text
                ArticleData['content'].append(articleElement)

            elif element.tag_name == "figure":
                articleElement = dict()
                try:

                    articleElement['type'] = "image"
                    articleElement['value'] = dict()


                    articleElement['value']['src'] =element.find_element_by_tag_name("img").get_attribute("src").split('?')[0]

                    articleElement['value']['caption'] =element.find_element_by_tag_name("img").get_attribute("alt")


                    ArticleData['content'].append(articleElement)
                except Exception as e:

                    print("image loading prblm", e, " \n line :", str(traceback.extract_stack()[-1][1]), "\n title ", link)

        return ArticleData

    def getCoverImage(self):

        ArticleElements=self.browser.find_element_by_xpath("//div[@class='gnt_ar_b']").find_elements_by_xpath("*")
        Cover = dict()
        Cover['src']=""
        Cover['caption']=""

        if ArticleElements[0].tag_name == "aside":
            Cover['src']=ArticleElements[0].find_element_by_tag_name("video").get_attribute("poster").split('?')[0]
            Cover['caption']=ArticleElements[0].find_element_by_tag_name("video").get_attribute("title")

        elif ArticleElements[0].tag_name == "figure":
            Cover['src']=ArticleElements[0].find_element_by_tag_name("img").get_attribute("src").split('?')[0]
            Cover['caption']=ArticleElements[0].find_element_by_tag_name("img").get_attribute("alt")

        elif "figure" in [i.tag_name for i in ArticleElements]:

            elements=self.browser.find_element_by_xpath("//div[@class='gnt_ar_b']").find_elements_by_xpath("*")
            for ele in elements:
                if ele.tag_name=="figure":

                    Cover['src']=ele.find_element_by_tag_name("img").get_attribute("src").split('?')[0]
                    Cover['caption']=ele.find_element_by_tag_name("img").get_attribute("alt")
                    break

        return Cover



