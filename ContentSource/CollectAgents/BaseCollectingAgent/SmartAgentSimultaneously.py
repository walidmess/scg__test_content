import traceback

from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException
from multiprocessing import Pool,freeze_support,Process
from ContentSource.DatabaseHandler.DB_Handler import db_handler

import json


class SmartAgentSimultanesly():

    def __init__(self, url, name):
        self.start = time.time()

        self.website = url
        self.name = name


    def CreateBrowser(self):
        opt = webdriver.ChromeOptions()
        opt.add_argument('--disable-notifications')
        opt.add_argument('--disable-blink-features=AutomationControlled')
        opt.add_argument("start-maximized")

        opt.add_argument("--headless")
        opt.add_argument('--no-sandbox')
        opt.add_argument('--disable-dev-shm-usage')

        opt.add_experimental_option("excludeSwitches", ["enable-automation"])
        opt.add_experimental_option('useAutomationExtension', False)

        # declare prefs
        prefs = {"credentials_enable_service": False,
                 "profile.password_manager_enabled": False}

        # add prefs
        opt.add_experimental_option("prefs", prefs)

        #browser = webdriver.Chrome(options=opt,executable_path="C:\\Users\\wezzy\\PycharmProjects\\LearnML\\chromedriver.exe")
        browser = webdriver.Chrome(options=opt,executable_path="/usr/bin/chromedriver")
        browser.execute_script(
            "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")

        return browser


    def OpenWebsite(self,browser):




        try:
            browser.get(self.website)
        # time.sleep(10)
        except:
            print("Cnx prblm , reloading the website")

            browser.get(self.website)

        return browser

    def ScanHomePage(self):

        pass

    def ReadArticle(self, link,browser):

        pass


    def ReadAllArticle(self,dbHandler):
        import json
        self.websiteData = dict()
        self.websiteData['name'] = self.website

        #self.websiteData['articles'] = []

        print(" found ",len(self.ArticlesLinks)," articles ")
        cleanLinks=self.FilterUrls(self.ArticlesLinks,dbHandler)
        #cleanLinks=self.ArticlesLinks

        print("Moving with ", len(cleanLinks))



        return  cleanLinks


    def ParalelWorker(self,link):
        print("Start Reading the article ",link)
        try:
            browser=self.CreateBrowser()

            dbHandler = db_handler()

            data = self.ReadArticle(link,browser)
            print("end reading article ",link)

            print("Start saving article ",link)

            self.SaveArticle(data,dbHandler)
            print("end saving article ",link)
        except:
            print("Problem in reading article ",link)
            browser.quit()

        browser.quit()

    def FilterUrls(self,newLinks,dbHandler):



        existedLinks=dbHandler.getAllUrls()

        CleanLinks=[]

        ForbidenWords=["review","reviews","deal","deals"]
        for url in newLinks:
            if url not in existedLinks:
                forbiden=False

                for word in url.split('/')[-1].split('-'):
                    if word in ForbidenWords:
                        forbiden=True
                        break

                if forbiden==False:
                    CleanLinks.append(url)



        #CleanLinks=[url for url in newLinks if url not in existedLinks ]

        return CleanLinks


    def SaveArticle(self,article,dbHandler):

        """
        with open(str(article['title'])+"-data.json", "w") as outfile:
            json.dump(self.websiteData, outfile, indent=4)

        """


        dbHandler.saveArticle(article['link'],article['cover'],article['category'], article['title'], article['content'], article['date'])

    def SaveArticles(self,dbHandler):

        #with open(str(self.name)+" data.json", "w") as outfile:
        #json.dump(self.websiteData, outfile, indent=4)

        from ContentSource.DatabaseHandler.DB_Handler import db_handler


        #dbHandler = db_handler()
        for article in self.websiteData['articles']:
            dbHandler.saveArticle(article['link'],article['cover']['src'], article['title'], article['content'], article['date'])


    def test(self):

        print("created well !")

    def StartWorking(self):

        dbHandler=db_handler()
        browser=self.CreateBrowser()

        self.OpenWebsite(browser)



        print("Start reaading the website")
        self.ScanHomePage(browser)

        links=self.ReadAllArticle(dbHandler)

        return links



