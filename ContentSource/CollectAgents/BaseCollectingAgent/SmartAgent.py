import traceback

from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException

import json


class SmartAgent():

    def __init__(self, url, name):
        self.start = time.time()

        self.website = url
        self.name = name



    def OpenWebsite(self):


        opt = webdriver.ChromeOptions()
        opt.add_argument('--disable-notifications')
        opt.add_argument('--disable-blink-features=AutomationControlled')
        opt.add_argument("start-maximized")

        opt.add_argument("--headless")
        opt.add_argument('--no-sandbox')
        opt.add_argument('--disable-dev-shm-usage')

        opt.add_experimental_option("excludeSwitches", ["enable-automation"])
        opt.add_experimental_option('useAutomationExtension', False)

        # declare prefs
        prefs = {"credentials_enable_service": False,
                 "profile.password_manager_enabled": False}

        # add prefs
        opt.add_experimental_option("prefs", prefs)

        #self.browser = webdriver.Chrome(options=opt,executable_path="C:\\Users\\wezzy\\PycharmProjects\\LearnML\\chromedriver.exe")
        self.browser = webdriver.Chrome(options=opt,executable_path="/usr/bin/chromedriver")
        self.browser.execute_script(
            "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")

        try:
            self.browser.get(self.website)
        # time.sleep(10)
        except:
            print("Cnx prblm , reloading the website")

            self.browser.get(self.website)

    def ScanHomePage(self):

        pass

    def ReadArticle(self, link):

        pass


    def ReadAllArticle(self):
        import json
        self.websiteData = dict()
        self.websiteData['name'] = self.website

        self.websiteData['articles'] = []

        print(" found ",len(self.ArticlesLinks)," articles ")
        cleanLinks=self.FilterUrls(self.ArticlesLinks)

        print("Moving with ", len(cleanLinks))

        for link in cleanLinks:
            print("Start Reading the article ",link)

            data = self.ReadArticle(link)

            #print(data)
            self.SaveArticle(data)
            #self.websiteData['articles'].append(data)
            #except Exception as e:
            #print("Saving article prblm",e, " \n line :", str(traceback.extract_stack()[-1][1]) ,"url ",link)


        #except Exception as e:
        #print("Reading article prblm",e, " \n line :", str(traceback.extract_stack()[-1][1]),"url ",link)


        self.browser.quit()
        self.end = time.time()

        print("End reading " + self.website +
              " | time = ", str(self.end - self.start))
        #self.SaveArticles()


    def FilterUrls(self,newLinks):

        from ContentSource.DatabaseHandler.DB_Handler import db_handler
        dbHandler = db_handler()

        existedLinks=dbHandler.getAllUrls()

        CleanLinks=[url for url in newLinks if url not in existedLinks ]

        return CleanLinks


    def SaveArticle(self,article):

        from ContentSource.DatabaseHandler.DB_Handler import db_handler
        dbHandler = db_handler()
        dbHandler.saveArticle(article['link'],article['cover'],article['category'], article['title'], article['content'], article['date'])

    def SaveArticles(self):

        #with open(str(self.name)+" data.json", "w") as outfile:
            #json.dump(self.websiteData, outfile, indent=4)

        from ContentSource.DatabaseHandler.DB_Handler import db_handler


        dbHandler = db_handler()
        for article in self.websiteData['articles']:
            dbHandler.saveArticle(article['link'],article['cover']['src'], article['title'], article['content'], article['date'])


    def test(self):

        print("created well !")

    def StartWorking(self):


        self.OpenWebsite()



        print("Start reaading the website")
        self.ScanHomePage()

        self.ReadAllArticle()
