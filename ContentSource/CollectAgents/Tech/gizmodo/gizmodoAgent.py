from ContentSource.CollectAgents.BaseCollectingAgent.SmartAgent import SmartAgent


class GizmodoAgent(SmartAgent):



    def ScanHomePage(self):

        articles = self.browser.find_elements_by_xpath("//article[@data-commerce-source='']")

        self.ArticlesLinks=[]
        for i in articles:
            self.ArticlesLinks.append(i.find_element_by_tag_name("h2").find_element_by_xpath("parent::a").get_attribute("href"))


    def ReadArticle(self,link):

        self.browser.get(link)

        ArticleData=dict()


        ArticleData['title'] = self.browser.find_element_by_tag_name("h1").text

        ArticleData['date'] = self.browser.find_element_by_tag_name("time").get_attribute("datetime")

        content = self.browser.find_element_by_xpath \
            ("//div[@class='r43lxo-0 ghTEBn js_post-content']"). \
            find_elements_by_tag_name("p")

        ArticleData['content']=""
        for c in content:
            ArticleData['content']=ArticleData['content']+"\n"+c.text



        return  ArticleData








