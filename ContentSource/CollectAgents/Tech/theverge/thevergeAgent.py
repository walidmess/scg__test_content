
from ContentSource.CollectAgents.BaseCollectingAgent.SmartAgent import SmartAgent
import traceback

from selenium.common.exceptions import NoSuchElementException


class TheVergeAgent(SmartAgent):

    def ScanHomePage(self):


        MainArticles = self.browser.find_elements_by_xpath(
            "//div[@class='c-entry-box-base c-entry-box-hero']")

        OtherArticles = self.browser.find_elements_by_xpath(
            "//div[@class='c-entry-box--compact c-entry-box--compact--article']")

        Articles = MainArticles + OtherArticles

        self.ArticlesLinks = [article.find_element_by_tag_name(
            "a").get_attribute('href') for article in Articles]

    def ReadArticle(self, link):


        try:
            self.browser.get(link)
        except:
            print("Cnx prblm , reloading the website")
            self.browser.get(link)


        ArticleData = dict()

        ArticleData['link']=link
        ArticleData['title'] = self.browser.find_element_by_tag_name("h1").text

        ArticleData['date'] = self.browser.find_element_by_tag_name(
            "time").get_attribute("datetime")

        ArticleData['category'] = self.browser.find_element_by_xpath("//div[contains(@class,'c-entry-group-labels')]").find_element_by_tag_name('ul').text.replace("\n", "/")


        ArticleData['cover'] = self.getCoverImage(link)

        list_article_children = self.browser.find_element_by_xpath(
            "//div[@class='c-entry-content ']").find_elements_by_xpath("*")

        ArticleData['content'] = self.ReadArticleElements(list_article_children)

        return ArticleData

    def getCoverImage(self, title=""):

        Cover = dict()
        Cover['src']=""
        Cover['caption']=""
        Cover['url']=""

        if self.checkElementExistance("//figure[@class='e-image e-image--hero']") ==True:

            if self.checkElementExistance("//figure[@class='e-image e-image--hero']/span[1]/span[1]/picture/img")==True:

                Cover['src'] = self.browser.find_element_by_xpath(
                    "//figure[@class='e-image e-image--hero']").find_element_by_tag_name("img").get_attribute("src")
            if self.checkElementExistance("//figure[@class='e-image e-image--hero']/span[2]")==True:

                Cover['caption'] = self.browser.find_element_by_xpath(
                    "//figure[@class='e-image e-image--hero']").find_element_by_xpath("//span[@class='e-image__meta']").text

        elif self.checkElementExistance("//div[@class='c-entry-hero__image']")==True :

                if self.checkElementExistance("//div[@class='c-entry-hero__image']/figure/span[1]/span[1]/picture/img"):
                    Cover['src'] = self.browser.find_element_by_xpath("//div[@class='c-entry-hero__image']").find_element_by_tag_name("img").get_attribute("src")

                if self.checkElementExistance("//div[@class='c-entry-hero__image']/figure/span[2]"):
                    Cover['caption'] = self.browser.find_element_by_xpath("//div[@class='c-entry-hero__image']").find_element_by_xpath("//span[@class='e-image__meta']").text

        elif self.checkElementExistance("//div[@class='l-col__main']/div[1]") ==True:
            Container=self.browser.find_element_by_xpath("//div[@class='l-col__main']/div[1]")
            if Container.get_attribute("class")=="c-video-embed  p-scalable-video":

                Cover['url']=self.browser.find_element_by_tag_name("iframe").get_attribute("src")

            if self.checkElementExistance("//meta[@property='og:image']"):
                Cover['src']=self.browser.find_element_by_xpath("//meta[@property='og:image']").get_attribute("content")

            if self.checkElementExistance("//meta[@property='og:description']"):
                Cover['caption']=self.browser.find_element_by_xpath("//meta[@property='og:description']").get_attribute("content")

        else:
            print("Cover image not found for article ",title)
    
        #request.urlretrieve(image, title+'-cover.'+image.split('.')[-1])
    

        return Cover

    def checkElementExistance(self,xpath):
        try:
            self.browser.find_element_by_xpath(xpath)

            return True

        except NoSuchElementException as e:
            return False




    def ReadArticleElements(self,list_article_children):
        list_childs=[]
        for element in list_article_children:
            articleElement = dict()

            if element.tag_name == "p":
                if element.text != '':
                    articleElement['type'] = "paragraph"
                    articleElement['value'] = element.text
                    list_childs.append(articleElement)

            elif element.tag_name == "figure":
                try:
                    #normal images

                    articleElement['type'] = "image"
                    articleElement['value'] = dict()
                    articleElement['value']['src'] = element.find_element_by_tag_name("span").find_element_by_tag_name("span").find_element_by_tag_name("picture").find_element_by_tag_name("img").get_attribute("src")
                    try:
                        articleElement['value']['caption'] = element.find_elements_by_xpath("span")[1].find_element_by_tag_name("figcaption").text
                    except:
                        print("No caption found !")
                        articleElement['value']['caption'] =""

                    list_childs.append(articleElement)
                except :

                    try:
                        print("Gif imaage found")
                        #Gif images
                        articleElement['type'] = "image"
                        articleElement['value'] = dict()
                        articleElement['value']['src'] = element.find_element_by_tag_name("span").find_element_by_tag_name("span").find_element_by_tag_name("img").get_attribute("src")
                        try:
                            articleElement['value']['caption'] = element.find_elements_by_xpath("span")[1].find_element_by_tag_name("figcaption").text

                        except:
                            print("No caption found !")

                            articleElement['value']['caption'] =""

                        list_childs.append(articleElement)
                    except Exception as e :
                        print("reading image prblm", e, " \n line :", str(traceback.extract_stack()[-1][1]))

            elif element.tag_name == "iframe":

                articleElement['type'] = "iframe"
                articleElement['value'] = element.get_attribute('src')

                list_childs.append(articleElement)


            elif element.tag_name in ["h1","h2","h3","h4","h5"]:
                articleElement['type'] = "Title"
                articleElement['value'] = {"size":element.tag_name,"content":element.text}

                list_childs.append(articleElement)


            elif element.tag_name == "div":

                attrs = list(self.browser.execute_script('var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;', element).keys())

                if "data-concert-ads-name" not in attrs:
                    res_children=self.ReadArticleElements(element.find_elements_by_xpath("*"))
                    if len(res_children)>0:
                        list_childs+=res_children









        return list_childs

