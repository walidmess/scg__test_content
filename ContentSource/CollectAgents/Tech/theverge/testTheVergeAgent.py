from ContentSource.CollectAgents.Tech.theverge.thevergeAgentSimultanesly import TheVergeAgentSimulatanesly
from multiprocessing import Process,Pool,cpu_count
import time


if __name__=="__main__":

    start=time.time()
    a = TheVergeAgentSimulatanesly("https://www.theverge.com/tech", "theVerge")


    cleanLinks=a.StartWorking()

    P=Pool(cpu_count())
    P.map(a.ParalelWorker,cleanLinks)

    """
    for l in cleanLinks:
        Job = Process(target=a.ParalelWorker,args=(l,))
        Job.start()
        Job.join()
    """
    end=time.time()

    print("finished at ",end-start)