
from ContentSource.CollectAgents.BaseCollectingAgent.SmartAgent import SmartAgent


class TheVentureBeatAgent(SmartAgent):



    def ScanHomePage(self):

        Articles=self.browser.find_elements_by_tag_name("article")


        self.ArticlesLinks=[article.find_element_by_tag_name("a").get_attribute("href") for article in Articles]



    def ReadArticle(self,link):

        self.browser.get(link)

        ArticleData = dict()

        ArticleData['title']  = self.browser.find_element_by_xpath \
            ("//h1[@class='article-title']").text

        ArticleData['date'] = self.browser.find_element_by_tag_name("time").get_attribute("datetime")

        articleContent = self.browser.find_element_by_xpath \
            ("//div[@class='article-content']"). \
            find_elements_by_tag_name("p")

        ArticleData['content'] = "\n ".join([p.text for p in articleContent if p.find_element_by_xpath("./..").get_attribute("class") == 'article-content'])

        return  ArticleData






