
from ContentSource.CollectAgents.BaseCollectingAgent.SmartAgent import SmartAgent


class TheWiredAgent(SmartAgent):



    def ScanHomePage(self):

        Articles = self.browser.find_elements_by_xpath \
            ("//li[contains(@class, 'card-component__description')]")

        Articles = Articles + self.browser.find_elements_by_xpath("//li[@class='post-listing-list-item__post']")


        self.ArticlesLinks = [article.find_element_by_tag_name("a").get_attribute("href") for article in Articles if article.find_element_by_tag_name('a').get_attribute('href').split('/')[3]=="story"]


    def ReadArticle(self,link):



        self.browser.get(link)

        ArticleData = dict()

        ArticleData['title'] = self.browser.find_element_by_tag_name("h1").text

        ArticleData['date'] = self.browser.find_element_by_tag_name("time").text

        articleContent = self.browser.find_element_by_xpath("//div[@class='grid--item body body__container article__body grid-layout__content']").find_elements_by_xpath("p")

        ArticleData['content'] = "\n ".join([p.text for p in articleContent ])








        return  ArticleData







