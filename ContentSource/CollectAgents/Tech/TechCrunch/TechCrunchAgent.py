
from ContentSource.CollectAgents.BaseCollectingAgent.SmartAgent import SmartAgent


class TechCrunchAgent(SmartAgent):



    def ScanHomePage(self):

        Articles = self.browser.find_elements_by_tag_name("article")

        self.ArticlesLinks = [ar.find_element_by_tag_name('a').get_attribute('href') for ar in Articles if 'events' not in ar.find_element_by_tag_name('a').get_attribute('href').split('/')]




    def ReadArticle(self,link):

        self.browser.get(link)

        ArticleData=dict()


        ArticleData['title'] = self.browser.find_element_by_xpath("//h1[@class='article__title']").text

        ArticleData['date'] = self.browser.find_element_by_tag_name("time").get_attribute("datetime")

        articleContent = self.browser.find_element_by_xpath \
            ("//div[@class='article-content']"). \
            find_elements_by_tag_name("p")



        ArticleData['content'] ="\n ".join( [p.text for p in articleContent])



        return  ArticleData






