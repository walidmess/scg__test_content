

import traceback

from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
import pickle
import json


opt = webdriver.ChromeOptions()
opt.add_argument('--disable-notifications')
opt.add_argument('--disable-blink-features=AutomationControlled')
opt.add_argument("start-maximized")

opt.add_experimental_option("excludeSwitches", ["enable-automation"])
opt.add_experimental_option('useAutomationExtension', False)

# declare prefs
prefs = {"credentials_enable_service": False, "profile.password_manager_enabled": False}

# add prefs
opt.add_experimental_option("prefs", prefs)

browser = webdriver.Chrome(options=opt,executable_path="C:\\Users\\wezzy\\PycharmProjects\\LearnML\\chromedriver.exe")
browser.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})");

browser.get("https://www.forbes.com/news")

