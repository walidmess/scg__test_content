import re



import traceback

from selenium import webdriver

import time


class PlagiaChecker:
    def __init__(self):

        pass

    def openBrowser(self):

        opt = webdriver.ChromeOptions()
        opt.add_argument('--disable-notifications')
        opt.add_argument('--disable-blink-features=AutomationControlled')
        opt.add_argument("start-maximized")

        opt.add_argument("--headless")
        opt.add_argument('--no-sandbox')
        opt.add_argument('--disable-dev-shm-usage')

        opt.add_experimental_option("excludeSwitches", ["enable-automation"])
        opt.add_experimental_option('useAutomationExtension', False)

        # declare prefs
        prefs = {"credentials_enable_service": False,
                 "profile.password_manager_enabled": False}

        # add prefs
        opt.add_experimental_option("prefs", prefs)

        browser = webdriver.Chrome(options=opt,executable_path="C:\\Users\\wezzy\\PycharmProjects\\LearnML\\chromedriver.exe")
        #browser = webdriver.Chrome(options=opt,executable_path="/usr/bin/chromedriver")
        browser.execute_script(
            "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")

        return browser

    def GoogleQuery(self,query_content):
        browser=self.openBrowser()
        browser.get("https://www.google.com")

        #find the search bar and send the qyery
        searchBar=browser.find_element_by_xpath("//input[@name='q']")
        searchBar.clear()
        searchBar.send_keys(query_content)
        browser.find_element_by_xpath("//input[@name='q']").submit()

        time.sleep(2)

        results=browser.find_elements_by_xpath("//div[@class='g']")
        links=[]
        #extracting the links
        for res in results:
            links.append(res.find_element_by_tag_name("a").get_attribute("href"))
            #print(res.find_element_by_tag_name("a").get_attribute("href"))
        browser.quit()
        return links

    def ValidateParaagraph(self,paragraph):
        VoidWords=['',' ','\n','\s','\t']
        paragraph=paragraph.strip()
        paragraph=paragraph.split(' ')

        if len(paragraph)>4:
            if paragraph not in VoidWords:
                return True

        return False


    def LinkExplore(self,link):
        browser=self.openBrowser()

        website=dict()
        try :
            browser.get(link)

            print("start scanning website",link)
            website=dict()
            website['link']=link
            website['content']=[p.text for p in browser.find_elements_by_tag_name("p") if self.ValidateParaagraph(p.text)==True]

            print("End scanning website ",link)
            return website

        except:
            print("prblm reading ",link)
            website['link']=link
            website['content']=[]
            return website

    def webResultsExplore(self,links):
        Results=[]
        for link in links:
            res=self.LinkExplore(link)
            if len(res)>0:
                Results.append(res)

        return Results

    def InternetPlagiatCheck(self,paragraph):
        links=self.GoogleQuery(paragraph)

        Results=self.webResultsExplore(links)
        GlobalRepport=[]

        if len(Results)>0:

            for res in Results:
                if len(res['content'])>0:
                    for p in res['content']:
                        matchedSeq=self.Compare(paragraph,p)
                        if len(matchedSeq)>0:
                            repport=dict()
                            repport['link']=res['link']
                            repport['matchedSeq']=matchedSeq
                            GlobalRepport.append(repport)
        return GlobalRepport

    def Compare(self,p1,p2,n=10):

        s1=self.generate_ngrams(p1,n)
        s2=self.generate_ngrams(p2,n)

        MatchedSeq=[]
        for i in s1:
            for j in s2:
                if i==j:
                    MatchedSeq.append((i,j))

        return MatchedSeq


    def generate_ngrams(self,s, n):
        s = s.lower()

        s = re.sub(r'[^a-zA-Z0-9\s]', ' ', s)

        tokens = [token for token in s.split(" ") if token != ""]


        ngrams = zip(*[tokens[i:] for i in range(n)])
        return [" ".join(ngram) for ngram in ngrams]



if __name__=="__main__":
    text="Twitter has announced that it will let you change who can respond to your tweets after you have posted it. You could already limit who replied to your tweets thanks to a feature that was widely distributed in August, but you had to set that preference while writing the tweet – with this update you can change who can respond to a later time — which could be a helpful way to reduce harassment. The feature will be available globally on iOS, Android and the web."

    A=PlagiaChecker()

    repport=A.InternetPlagiatCheck(text)

    for i in repport:
        print(i)

