import traceback

from selenium import webdriver
import time


class CeoTitleAgent:

    def OpenWebSite(self):

        opt = webdriver.ChromeOptions()

        opt.add_argument('--disable-notifications')
        opt.add_argument('--disable-blink-features=AutomationControlled')
        opt.add_argument("start-maximized")

        #opt.add_argument("--headless")
        opt.add_argument('--no-sandbox')
        opt.add_argument('--disable-dev-shm-usage')

        opt.add_experimental_option("excludeSwitches", ["enable-automation"])
        opt.add_experimental_option('useAutomationExtension', False)

        # declare prefs
        prefs = {"credentials_enable_service": False,
                 "profile.password_manager_enabled": False}

        # add prefs
        opt.add_experimental_option("prefs", prefs)

        self.browser = webdriver.Chrome(options=opt,executable_path="C:\\Users\\wezzy\\PycharmProjects\\LearnML\\chromedriver.exe")
        #self.browser = webdriver.Chrome(options=opt,executable_path="/usr/bin/chromedriver")
        self.browser.execute_script(
            "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")


        self.browser.get("https://headlines.coschedule.com/headlines")

